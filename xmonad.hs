--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--
import XMonad
import XMonad.Layout.Fullscreen
    ( fullscreenEventHook, fullscreenManageHook, fullscreenSupport, fullscreenFull )
import Data.Monoid
import System.Exit ()
import XMonad.Util.SpawnOnce ( spawnOnce )
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import Graphics.X11.ExtraTypes.XF86 (xF86XK_AudioLowerVolume, xF86XK_AudioRaiseVolume, xF86XK_AudioMute, xF86XK_MonBrightnessDown, xF86XK_MonBrightnessUp, xF86XK_AudioPlay, xF86XK_AudioPrev, xF86XK_AudioNext)
import XMonad.Hooks.EwmhDesktops ( ewmh )
import Control.Monad ( join, when )
import XMonad.Layout.NoBorders
import XMonad.Hooks.ManageDocks
    ( avoidStruts, docks, manageDocks, Direction2D(D, L, R, U), ToggleStruts(..) )
import XMonad.Hooks.ManageHelpers ( doFullFloat, isFullscreen, doCenterFloat )
--import XMonad.Layout.Spacing ( spacingRaw, Border(Border) )
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
    ( Direction2D(D, L, R, U),
      gaps,
      setGaps,
      GapMessage(DecGap, ToggleGaps, IncGap) )

import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import Data.Maybe (maybeToList)

-- My CHANGE
import XMonad.Actions.FindEmptyWorkspace
import XMonad.Hooks.WindowSwallowing
import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect
import XMonad.Util.EZConfig (mkNamedKeymap, checkKeymap)
import XMonad.Util.NamedActions
import System.IO (hClose, hPutStr, hPutStrLn)
import Data.Char (isSpace, toUpper)
import XMonad.Util.Run (spawnPipe)

-- actions
import XMonad.Actions.CopyWindow
import XMonad.Actions.UpdatePointer -- update mouse postion

import XMonad.Layout.ShowWName
import XMonad.Layout.NoBorders

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"


-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal :: String
myTerminal = "alacritty"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask


executeIfTrue :: Bool -> IO () -> IO ()
executeIfTrue b cmd = if b then cmd else return ()

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
  { swn_font              = "xft:Ubuntu:bold:size=60"
  , swn_fade              = 1.0
  , swn_bgcolor           = "#1c1f24"
  , swn_color             = "#ffffff"
  }


-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]

-- myWorkspaces = [" dev ", " www ", " sys ", " doc ", " vbox ", " chat ", " mus ", " vid ", " gfx "]

-- myWorkspaces = ["term", "web", "mail", "music"] ++ (map show [5..9])

-- myWorkspaces    = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

-- myWorkspaces    = ["\63083", "\63288", "\63306", "\61723", "\63107", "\63601", "\63391", "\61713", "\61884"]

-- myWorkspaces = ["\63083", "\63288", "sys", "dev"] ++ (map show [5..9])


--myWorkspaces = ["term", "www", "sys", "doc", "dev", "cry"] ++ (map show [7..9])
-- myWorkspaces :: [[Char]]
-- myWorkspaces :: [String]
myWorkspaces    = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)


-- Border colors for unfocused and focused windows, respectively.
--

  
myNormalBorderColor :: String
myNormalBorderColor  = "#3b4252"
myFocusedBorderColor :: String
myFocusedBorderColor = "#bc96da"

-- CUSTOM SELECT GRID TO BUILD 

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), cancel)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_k)     , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         ,((0,xK_space) , setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                (0x28,0x2c,0x34) -- lowest inactive bg
                (0x28,0x2c,0x34) -- highest inactive bg
                (0xc7,0x92,0xea) -- active bg
                (0xc0,0xa7,0x9a) -- inactive fg
                (0x28,0x2c,0x34) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 200
    , gs_cellpadding  = 6
    , gs_navigate    = myNavigation
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 40
                   , gs_cellwidth    = 180
                   , gs_cellpadding  = 6
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   }

runSelectedAction' :: GSConfig (X ()) -> [(String, X ())] -> X ()
runSelectedAction' conf actions = do
    selectedActionM <- gridselect conf actions
    case selectedActionM of
        Just selectedAction -> selectedAction
        Nothing -> return ()



addNETSupported :: Atom -> X ()
addNETSupported x   = withDisplay $ \dpy -> do
    r               <- asks theRoot
    a_NET_SUPPORTED <- getAtom "_NET_SUPPORTED"
    a               <- getAtom "ATOM"
    liftIO $ do
       sup <- (join . maybeToList) <$> getWindowProperty32 dpy a_NET_SUPPORTED r
       when (fromIntegral x `notElem` sup) $
         changeProperty32 dpy r a_NET_SUPPORTED a propModeAppend [fromIntegral x]

addEWMHFullscreen :: X ()
addEWMHFullscreen   = do
    wms <- getAtom "_NET_WM_STATE"
    wfs <- getAtom "_NET_WM_STATE_FULLSCREEN"
    mapM_ addNETSupported [wms, wfs]

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
clipboardy :: MonadIO m => m () -- Don't question it 
clipboardy = spawn "rofi -modi \"\63053 :greenclip print\" -show \"\63053 \" -run-command '{cmd}' -theme ~/.config/rofi/launcher/style.rasi"

centerlaunch = spawn "exec ~/bin/eww open-many blur_full weather profile quote search_full disturb-icon vpn-icon home_dir screenshot power_full reboot_full lock_full logout_full suspend_full"
sidebarlaunch = spawn "exec ~/bin/eww open-many weather_side time_side smol_calendar player_side sys_side sliders_side"
ewwclose = spawn "exec ~/bin/eww close-all"

-- TODO Figure out why not working since update
-- maimcopy = spawn "maim -s | xclip -selection clipboard -t image/png && notify-send \"Screenshot\" \"Copied to Clipboard\" -i flameshot"
-- maimsave = spawn "maim -s ~/Desktop/$(date +%Y-%m-%d_%H-%M-%S).png && notify-send \"Screenshot\" \"Saved to Desktop\" -i flameshot"
rofi_launcher = spawn "rofi -no-lazy-grab -show drun -modi run,drun,window -theme $HOME/.config/rofi/launcher/style -drun-icon-theme \"candy-icons\" "

------------------------------------------------------------------------
-- My custom App
--
my_qrcode_shadow = spawn "exec ~/bash_scripts/qrcode/qrcode-shadow.sh"

--myHiddenBarHook = do
--	let b = True
--	executeIfTrue b (putStrLn "Commande exécutée")
-- myDisplayBar =? True        $ sendMessage ToggleGaps

-- executeIfTrue myDisplayBar (spawn "sendMessage ToggleGaps")
-- executeIfTrue myDisplayBar (putStrLn "ommande exécutée")

-- myDisplayBar
--	executeIfTrue myDisplayBar sendMessage ToggleGaps)
  -- spawnOnce "exec ~/bin/bartoggle"
  -- To hide bartoggle
  -- sendMessage ToggleGaps
  
setBarToogle = do
   -- layoutHook = gaps [(U,40), (D,60)] $ spacingRaw True (Border 0 0 0 0) True (Border 5 5 5 5) True $ smartBorders $ myLayout
   spawnOnce "exec ~/bin/bartoggle"

subtitle' ::  String -> ((KeyMask, KeySym), NamedAction)
subtitle' x = ((0,0), NamedAction $ map toUpper
                      $ sep ++ "\n-- " ++ x ++ " --\n" ++ sep)
  where
    sep = replicate (6 + length x) '-'


showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
  h <- spawnPipe $ "yad --text-info --fontname=\"SauceCodePro Nerd Font Mono 12\" --fore=#46d9ff back=#282c36 --center --geometry=1200x800 --title \"XMonad keybindings\""
  --hPutStr h (unlines $ showKm x) -- showKM adds ">>" before subtitles
  hPutStr h (unlines $ showKmSimple x) -- showKmSimple doesn't add ">>" to subtitles
  hClose h
  return ()


myKeys :: XConfig l0 -> [((KeyMask, KeySym), NamedAction)]
myKeys c =
  --(subtitle "Custom Keys":) $ mkNamedKeymap c $
  let subKeys str ks = subtitle' str : mkNamedKeymap c ks in
  subKeys "Xmonad Essentials"
  [
  ("M-S-<Return>", addName "Run prompt" $ spawn (myTerminal))
  -- , ("M-S-c", addName "Kill process" $ kill)
  , ("M1-<F4>", addName "Kill process" $ kill)
  , ("M-q", addName "Restart Xmonad" $ spawn "xmonad --restart")

  -- lock screen
  , ("M-L", addName "Lock Screen" $ spawn "betterlockscreen -l")

  -- launch rofi and dashboard
  , ("M-o", addName "" $ rofi_launcher)
  , ("M-p", addName "Open eww sidebar" $ centerlaunch)
  ,("M-S-p", addName "Open eww sidebar" $ ewwclose)

  -- launch eww sidebar
  , ("M-s", addName "Open eww sidebar" $ sidebarlaunch)
  , ("M-S-s", addName "Close eww sidebar" $ ewwclose)
  , ("M-z", addName "inhibit_activate" $ spawn "exec ~/bin/inhibit_activate")
  , ("M-S-z", addName "inhibit_deactivate" $ spawn "exec ~/bin/inhibit_deactivate")
  , ("M-d", addName "Turn off message" $ spawn "exec ~/bin/do_not_disturb.sh")
  , ("M-a", addName "Clipboars" $ clipboardy)
  ]
  
  ^++^ subKeys "My Custom Function"
  [
   ("M-r", addName "Generate QrCode from clipboard" $ my_qrcode_shadow)
  ]

  ^++^ subKeys "Gapes"
  [
   ("M-M1-<Space>", addName "toggle all gaps" $ sendMessage ToggleGaps)
  ]
  
  -- Floating windows
  ^++^ subKeys "Floating windows"
  [ 
  ("M-t", addName "Sink a floating window" $ withFocused $ windows . W.sink)
  , ("M-<Space>", addName "Switch to  layout" $ sendMessage NextLayout)
  , ("M-<F11>", addName "Switch to next layout" $ sendMessage NextLayout)
  , ("M-<F12>", addName "display selection grid" $ goToSelected $ mygridConfig myColorizer)
  
  ]
  
  ^++^ subKeys "Special Key"
  [
  -- Audio keys
    ("<xF86AudioPlay>", addName "Play/Stop sound" $ spawn "playerctl play-pause")
    ,("<xF86AudioPrev>", addName "Previous sound" $ spawn "playerctl previous")
    ,("<xF86AudioNext>", addName "Next sound"$ spawn "playerctl next")
    ,("<XF86AudioRaiseVolume>", addName "Raise sound" $ spawn "pactl set-sink-volume 0 +5%")
    ,("<XF86AudioLowerVolume>", addName "Low sound" $ spawn "pactl set-sink-volume 0 -5%")
    ,("<xF86AudioMute>", addName "Mute Sound " $ spawn "pactl set-sink-mute 0 toggle")
    -- Brightness keys
    ,("<XF86MonBrightnessUp>", addName "Up sound" $ spawn "brightnessctl s +10%")
    ,("<XF86MonBrightnessDown>", addName "Down sound" $ spawn "brightnessctl s 10-%")

  ]
        
  ^++^ subKeys "Switch to workspace"
  [
  ("M-<Right>", addName "Move window to next WS" $ nextWS)
  -- , ("M-S-l", addName "toogle all workspace" $ toggleGapsOnAllWS)
  , ("M-<Left>", addName "Move window to next WS" $ prevWS)
  , ("M-S-<Right>", addName "Move window to next WS" $ shiftToNext)
  , ("M-S-<Left>", addName "Move window to next WS" $ shiftToPrev)

  ]
  
  ^++^ subKeys "Monitors"
  [ ("M-<Up>", addName "Switch focus to next monitor" $ nextScreen)
  , ("M-<Down>", addName "Switch focus to prev monitor" $ prevScreen)
  , ("M-S-<Up>", addName "Move client to next monitor" $ shiftNextScreen)
  , ("M-S-<Down>", addName "Move client to prev monitor" $ shiftPrevScreen)
  , ("M-C-a", addName "copy window to all workspaces" $ windows copyToAll)
  , ("M-C-z", addName "kill copies of window on other workspaces" $ killAllOtherCopies)
  ]
  
  ^++^ subKeys "Move to n Workspaces" (
  [ ("M-<F" ++ i ++ ">", addName ("Switch to workspace " ++ i) (windows $ W.view i)) 
  | (i) <- myWorkspaces]
  )
  
  ^++^ subKeys "Move client to n Workspaces" (
  [ ("M-S-<F" ++ i ++ ">", addName ("Move client to Workspace " ++ i) (windows $ W.shift i)) 
  | (i) <- myWorkspaces]
  )
  
  
  -- TODO
  -- TRY Keybinding with Numpad
  
  -- $ xev to find keysym name
  -- mine is KP_0, KP_1
  
  -- https://wiki.haskell.org/Xmonad/Frequently_asked_questions#Numeric_keypad_keys_like_xK_KP_2_not_working
  
  -- https://stackoverflow.com/questions/73404806/move-window-to-workspace-and-focus-that-workspace-shiftandview-on-xmonad-wm
  
  -- https://github.com/nyergler/xmonad-conf/blob/master/xmonad.hs

  
   -- ^++^ subKeys "Monitors"
   -- [ ("M-" ++ "<xK_KP_" ++ k ++ ">", addName "Switch windows" $ windows $ f i)
   --     | (i, k) <- zip myWorkspaces ["End","Down","Next","Left","Begin","Right","Home","Up","Prior"]
   --     , (f, m) <- [(W.greedyView, ""), (W.shift, "S-"), (\w -> W.greedyView w . W.shift w, "C-")]
   --     ]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]
    
mySpacing = spacingRaw True (Border 3 3 3 3) True (Border 3 3 3 3) True

-- mySpacingForBar = spacingRaw True (Border 3 3 3 3) True (Border 3 3 3 3) True

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--

myTall = Tall 1 (3/100) (1/2)

myLayout = avoidStruts(tiled ||| Mirror tiled ||| noBorders Full)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = mySpacing $ myTall



------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = fullscreenManageHook <+> manageDocks <+> composeAll
    [ 
    -- className =? "Chromium"       --> doShift ( myWorkspaces !! 1 )
    -- , className =? "firefox"        --> doShift ( myWorkspaces !! 1 )
    className =? "dolphin"        --> doShift "doc"
    , className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doCenterFloat
    , className =? "burp-StartBurp" --> doCenterFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    , isFullscreen --> doFullFloat
                                 ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
-- myEventHook = mempty


------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()

   
------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--


myStartupHook = do
  -- DISABLE => to display bartoggle when I want
  -- spawnOnce "exec ~/bin/bartoggle"
  
  spawnOnce "exec ~/bin/eww daemon"
  spawn "xsetroot -cursor_name left_ptr"
  
  -- maybe change to spawnOne
  spawn "exec ~/bin/lock.sh"
  
  spawn "feh --bg-fill ~/wallpapers/arch-linux-2.png ~/wallpapers/arch-linux-2.png"
  spawnOnce "picom --experimental-backends"
  spawnOnce ".config/xmonad/scripts/autostart.sh"
  spawnOnce "greenclip daemon"
  spawnOnce "dunst"

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main :: IO ()
main = do 

	xmonad $ addDescrKeys ((mod4Mask, xK_m), showKeybindings) myKeys $  fullscreenSupport $ docks $ ewmh $ def 
		{
		manageHook = myManageHook <+> manageDocks,
	      -- simple stuff
		terminal           = myTerminal,
		focusFollowsMouse  = myFocusFollowsMouse,
		clickJustFocuses   = myClickJustFocuses,
		borderWidth        = myBorderWidth,
		modMask            = myModMask,
		workspaces         = myWorkspaces,
		normalBorderColor  = myNormalBorderColor,
		focusedBorderColor = myFocusedBorderColor,

	      -- key bindings
		mouseBindings      = myMouseBindings,

	      -- hooks, layouts
		
		-- DISABLE
		-- layoutHook = gaps [(U,40), (D,60)] $ spacingRaw True (Border 0 0 0 0) True (Border 5 5 5 5) True $ smartBorders $ myLayout,
		
		-- layoutHook = gaps [(L,0), (R,0), (U,0), (D,5)] $ spacingRaw True (Border 0 0 0 0) True (Border 5 5 5 5) True $ smartBorders $ myLayout,

		-- layoutHook = spacingRaw True (Border 0 10 10 10) True (Border 10 10 10 10) True $ myLayoutHook,
		
		layoutHook         = showWName' myShowWNameTheme $ myLayout,
		
		-- handleEventHook    = myEventHook,
		handleEventHook = swallowEventHook (className =? "Alacritty"  <||> className =? "kitty") (return True),
		startupHook        = myStartupHook >> addEWMHFullscreen,
		logHook            = myLogHook >> updatePointer (0.5, 0.5) (1, 1)
	    }

